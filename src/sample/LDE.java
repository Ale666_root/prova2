package sample;


public class LDE {
    No primeiro;
    No ultimo;
    int qtd;

    public No getPrimeiro() {
        return primeiro;
    }

    public void setPrimeiro(No primeiro) {
        this.primeiro = primeiro;
    }

    public No getUltimo() {
        return ultimo;
    }

    public void setUltimo(No ultimo) {
        this.ultimo = ultimo;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    public boolean isvazia() {
        return (qtd == 0);
    }

    public No removerInicio() {
        No aux = this.primeiro;
        if (qtd == 0) {
            System.out.println("lista Vazia!!");
        } else {

            aux = aux.getProx();
            primeiro = aux;
            qtd--;
        }
        return aux;
    }
    public void removerFim() {
        if (qtd == 0) {
            System.out.println("lista Vazia!!");
        }
        No aux = this.ultimo.getAnt();
        aux.setProx(null);
        ultimo = aux;
        qtd--;

    }

    public void addInicio(Object o) {
        No novo = new No(o);
        if (qtd == 0) {
            this.primeiro = novo;
            this.ultimo = novo;
            qtd++;
        } else {
            novo.setProx(primeiro);
            primeiro.setAnt(novo);
            primeiro = novo;
            qtd++;
        }

    }

    public void addFim(Object o) {
        No novo = new No(o);
        if (qtd == 0) {
            this.primeiro = novo;
            this.ultimo = novo;
            qtd++;
        } else {
            this.ultimo.setProx(novo);
            novo.setAnt(ultimo);
            ultimo = novo;
            qtd++;
        }

    }

    private void addMeio(Object o) {
        No aux = primeiro;
        No ant = aux;
        No novo = new No(o);
        for (int i = 0; i < qtd / 2; i++) {
            ant = aux;
            aux = aux.getProx();
        }
        /*System.out.println(ant.dado);
        System.out.println(aux.dado);*/

        qtd++;

    }

    private void remove(int posicao) {
        if (!this.posicaoOcupada(posicao)) {
            throw new IllegalArgumentException("Posição não existe");
        }

        if (posicao == 0) {
            this.removerInicio();
        } else if (posicao == this.qtd - 1) {
            this.removerFim();
        } else {
            No anterior = this.pegaCelula();
            No atual = anterior.getProx();
            No proxima = atual.getProx();

            anterior.setProx(proxima);
            proxima.setAnt(anterior);

            this.qtd--;
        }
    }

    public void mostrar() {
        No no = this.primeiro;
        for (int i = 0; i < qtd; i++) {
            System.out.println(no.dado);
            no = no.getProx();
        }
    }

    private void mostrarInverso() {
        No no = this.ultimo;
        while (no != null) {
            System.out.println(no.dado);
            no = no.getAnt();
        }
    }

    private boolean posicaoOcupada(int posicao) {
        return posicao >= 0 && posicao < this.qtd;
    }

    private No pegaCelula() {
        if (!this.posicaoOcupada(qtd / 2)) {
            throw new IllegalArgumentException("Posição não existe");
        }

        No atual = primeiro;
        for (int i = 0; i < (qtd / 2) - 1; i++) {
            atual = atual.getProx();
        }
        return atual;
    }

}
