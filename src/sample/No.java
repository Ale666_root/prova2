package sample;

public class No {
    Object dado;
    No Prox;
    No ant;

    public No(Object dado) {
        this.dado = dado;
    }

    public Object getDado() {
        return dado;
    }

    public void setDado(Object dado) {
        this.dado = dado;
    }

    public No getProx() {
        return Prox;
    }

    public void setProx(No prox) {
        Prox = prox;
    }

    public No getAnt() {
        return ant;
    }

    public void setAnt(No ant) {
        this.ant = ant;
    }
}
